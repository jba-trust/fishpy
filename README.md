Creative Commons Attribution 4.0 International (CC BY 4.0)

**The fishPy tool: An individual-based model to predict upstream movements of brown trout (Salmo trutta)**

Citation: 
Padgett, Thomas (2020) The fishPy tool: An individual-based model to predict upstream movements of brown trout (Salmo trutta). University of Leeds. [Dataset] https://doi.org/10.5518/861

Dataset description:
The fishPy tool is a three-dimensional, spatially-continuous, individual-based model of fish responses to hydraulic stimuli during upstream migration through ecohydraulic environments.

This is a copy of the original repo at URI:	https://archive.researchdata.leeds.ac.uk/id/eprint/754

Divisions:	Faculty of Engineering > Centre for Computational Fluid Dynamics

Date deposited:	13 Oct 2020 07:58

This dataset contains the fishPy tool. The fishPy tool is a three-dimensional, spatially-continuous, individual-based model of fish responses to hydraulic stimuli during upstream migration through ecohydraulic environments. 

These data were produced as part of the PhD thesis:
Development and Application of Individual-based Models for Predicting Upstream Passage of European Fish undertaken by Thomas E. Padgett at the University of Leeds. 

This repository contains the following files:

fishPy.py - The individual-based model.
This model requires an input such as the examples given in /domains
A new input can be created using the data provided in OF2fishPy.zip.

outputs.zip contains example outputs from the fishPy tool.

Once downloaded, place all files in the same folder and unzip any compressed folders. This is required as the fishPy tool will search for the /domains and /outputs folders.
